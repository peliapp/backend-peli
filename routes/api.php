<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* 
    Route::resource('photo', 'PhotoController', ['only' => [
    'index', 'show'
        ]]);

*/

Route::get('/Pelicula', 'PeliculaController@index');

Route::post('/GuardarPelicula', 'PeliculaController@create');

Route::post('/cambiarEstadoPelicula', 'PeliculaController@cambiarEstado');
Route::post('/EliminarPelicula', 'PeliculaController@EliminarPelicula');

Route::post('/listaTurno', 'PeliculaController@listardTurno');
Route::post('/activandacheckdp', 'PeliculaController@activandacheckdp');

Route::post('/EditaPelicula', 'PeliculaController@EditaPelicula');

// turno 


Route::get('/listarturno', 'turnoController@index');
Route::post('/guardarturno', 'turnoController@create');
Route::post('/editarturno', 'turnoController@edit');
Route::post('/editaestado', 'turnoController@editEstado');
Route::post('/eliminarturno', 'turnoController@Eliminarturno');

