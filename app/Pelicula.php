<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    //
    public $timestamps = false;

    protected $table = 'pelicula';
    protected $primaryKey = 'idPelicula';
    protected $fillable = [
        'nombre',
        'publicacion',
        'urlImagen',
        'created_at',
        'update_at',
        'idestado'
    ];
}
