<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class turno extends Model
{
    //

    public $timestamps = false;

    protected $table = 'turno';
    protected $primaryKey = 'idturno';
    protected $fillable = [
        'varfechturno',
        'idestado'
        
    ];

}
