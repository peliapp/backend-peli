<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detallepelicula extends Model
{
    //
    public $timestamps = false;

    protected $table = 'detalle_pelicula';
    protected $fillable = [
        'idturno',
        'idPelicula',
        'valor',
        
    ];


}
