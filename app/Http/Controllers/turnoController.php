<?php

namespace App\Http\Controllers;

use App\detallepelicula;
use App\Pelicula;
use App\turno;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class turnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       
        
        $list_turno = turno::join('estado', 'turno.idestado', '=', 'estado.idestado')
                        ->select('turno.idturno','turno.varfechturno','turno.idestado', 'estado.varNombrEstado')->get();
        
        return response()->json(['message' => 'Si se ha encontrado dato', 'data' => $list_turno]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $varhora = $request->input('varfechturno');
        $varboolean = $request->input('estado');

        $pelicula_existe=Pelicula::select('idPelicula','publicacion','idestado')->get();

        if ($varboolean=="false") {
                $obtenerIDturno=turno::create([
                'varfechturno'=>$varhora,
                'idestado'=>2
                ]);
        } else {
                $obtenerIDturno=turno::create([
                    'varfechturno'=>$varhora,
                    'idestado'=>1
                    ]);
        }

        if(count($pelicula_existe)>0){
           
            foreach ($pelicula_existe as $clave => $valor) {
                //echo $valor['idturno'];
                detallepelicula::create([
                    'idturno'=>$obtenerIDturno['idturno'],
                    'idPelicula' =>$valor['idPelicula'],
                    'valor' => 1,  
                ]);

            }
        }
     
      
       return response()->json(['message' => 'Guardado Correctamente']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
     
        $idturno = $request->input('idturno');
        $varhora = $request->input('varfechturno');
        $varboolean = $request->input('estado');

        if ($varboolean=="false") {
                    turno::where('idturno', '=', $idturno)
                    ->update([
                    'varfechturno'=>$varhora,
                    'idestado'=>2
                    ]);
                   
        } else {
                    turno::where('idturno', '=', $idturno)
                    ->update([
                        'varfechturno'=>$varhora,
                        'idestado'=>1
                        ]);
        }
      
       return response()->json(['message' => 'Actualizado Correctamente']);

    }

    public function editEstado(Request $request)
    {
        //
     
        $idturno = $request->input('idturno');
        
        $estado= turno::where('idturno', '=', $idturno)->first(['idestado']);

        if( $estado['idestado'] === 2){
            turno::where('idturno', '=', $idturno)
            ->update([
                'idestado'=>1
                ]);
        }else{
            turno::where('idturno', '=', $idturno)
            ->update([
                'idestado'=>2
                ]);
        }
            

       return response()->json(['message' => 'Actualizado Correctamente']);

    }

    public function Eliminarturno(Request $request)
    {
        //
     
        $idturno = $request->input('idturno');

        turno::where('idturno', '=', $idturno)->delete();
       
       return response()->json(['message' => 'Eliminado Correctamente']);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
