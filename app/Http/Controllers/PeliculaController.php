<?php

namespace App\Http\Controllers;

use App\detallepelicula;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pelicula;
use App\turno;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


//use App\File;


class PeliculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pelicula = Pelicula::join('estado', 'pelicula.idestado', '=', 'estado.idestado')
            ->select('pelicula.idPelicula', 'pelicula.nombre', 'pelicula.publicacion', 'pelicula.urlImagen', 'pelicula.created_at', 'pelicula.update_at', 'pelicula.idestado', 'estado.varNombrEstado')->get();

        //$users = DB::table('pelicula')->get();

        //dd($pelicula);
        return response()->json(['message' => 'Si se ha encontrado dato', 'data' => $pelicula]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // return response()->json(['name' => $request->get('file') ]);

        $varnombre = $request->get('nombre');
        $varpublica = $request->get('publicacion');
        $varUImagen =  $request->file('file');
        $path="";
        if ($varUImagen) {
            // $file = $request->file->store('public');
            // $image = base64_decode($request->get('file'));
            $path = Storage::putFile('public',  $varUImagen);
            $pru = "entre";
            // return response()->json(['message' => $request->get('input_name'), 'name' => $file ->getClientOriginalExtension()]);
            // return response()->json(['name' => $varnombre,'publicacion' => $varpublica, 'url' => $path]);
        }
        // $extension = 'jpg';
        // $fileName =  $varnombre . '.' . $extension;
        // $path = public_path('images/users/',$fileName);



        // Storage::disk('uploads')->put($fileName, $varUImagenn);

        //  Storage::disk($path)->put($fileName, $varUImagenn);
        //return  Storage::putFile('images/users/',$varUImagenn);


        date_default_timezone_set('America/Lima'); // CDT


         $turno_existe=turno::select('idturno','varfechturno','idestado')->get();

        $regi_pelicula = Pelicula::create([
            'nombre'=>$varnombre,
            'publicacion' => $varpublica,
            'urlImagen' => $path,
            'created_at' => $current_date = date('Y/m/d H:i:s'),
            'idestado'=>1,
        ]);


        if(count($turno_existe)>0){
            foreach ($turno_existe as $clave => $valor) {
                //echo $valor['idturno'];
                detallepelicula::create([
                    'idturno'=>$valor['idturno'],
                    'idPelicula' =>$regi_pelicula['idPelicula'],
                    'valor' => 1,  
                ]);


            }
        }

        return response()->json(['message' =>"Guardado Satisfactorio"]);

    }


    public function EditaPelicula(Request $request)
    {
        // return response()->json(['name' => $request->get('file') ]);


        $idPelicula = $request->get('idPelicula');
        $varnombre = $request->get('nombre_edit');
        $varpublica = $request->get('fech_publ_edit');
        $varUImagen =  $request->file('file_edit');
        $path="";



        date_default_timezone_set('America/Lima'); // CDT


        if ($varUImagen) {
           
            $path = Storage::putFile('public',  $varUImagen);
            $pru = "entre";
            
            Pelicula::where('idPelicula', '=', $idPelicula)
            ->update([
                'nombre'=>$varnombre,
                'publicacion' => $varpublica,
                'urlImagen' => $path,
                'update_at' => $current_date = date('Y/m/d H:i:s'),
                
            ]);
        }else{
            Pelicula::where('idPelicula', '=', $idPelicula)
            ->update([
                'nombre'=>$varnombre,
                'publicacion' => $varpublica,
                'update_at' => $current_date = date('Y/m/d H:i:s'),
                
            ]);


        }



        return response()->json(['message' =>"Actualizacion Satisfactorio"]);

    }




    public function cambiarEstado(Request $request)
    {
        date_default_timezone_set('America/Lima'); // CDT
        $idpelicula = $request->input('idPelicula');

        $estado = Pelicula::where('idPelicula', '=', $idpelicula)->first(['idestado']);

        if ($estado['idestado'] === 2) {
            Pelicula::where('idPelicula', '=', $idpelicula)
                ->update([
                    'idestado' => 1,
                    'update_at' => $current_date = date('Y/m/d H:i:s')
                ]);
        } else {
            Pelicula::where('idPelicula', '=', $idpelicula)
                ->update([
                    'idestado' => 2,
                    'update_at' => $current_date = date('Y/m/d H:i:s')
                ]);
        }

        return response()->json(['message' => 'Guardado Con exito']);
    }

    public function EliminarPelicula(Request $request)
    {


        $idpelicula = $request->input('idPelicula');

        Pelicula::where('idPelicula', '=', $idpelicula)->delete();

        return response()->json(['message' => 'Eliminado Correctamente']);
    }


    public function ObtenerTurnoPelicula(Request $request)
    {


        $idpelicula = $request->input('idPelicula');

        Pelicula::where('idPelicula', '=', $idpelicula)->delete();

        return response()->json(['message' => 'Eliminado Correctamente']);
    }

    public function listardTurno(Request $request)
    {


        $idpelicula = $request->input('idPelicula');

        $obtnerdatella = detallepelicula::join('turno', 'turno.idturno', '=', 'detalle_pelicula.idturno')
            ->where('detalle_pelicula.idPelicula', '=', $idpelicula)
            ->where('turno.idestado', '=', 1)
            ->select('turno.varfechturno', 'turno.idturno', 'detalle_pelicula.valor', 'detalle_pelicula.idPelicula')->get();


        return response()->json(['message' => 'Guardado Con exito', 'data' => $obtnerdatella]);
    }

    public function activandacheckdp(Request $request)
    {


        $idpelicula = $request->input('idPelicula');
        $idturno = $request->input('idturno');
        $valor = $request->input('valor');

        if ($valor == "1") {
            detallepelicula::where('idPelicula', '=', $idpelicula)
                ->where('idturno', '=', $idturno)
                ->update([
                    'valor' => 2,

                ]);
        } else {
            detallepelicula::where('idPelicula', '=', $idpelicula)
                ->where('idturno', '=', $idturno)
                ->update([
                    'valor' => 1,

                ]);
        }


        return response()->json(['message' => 'Encontro Data']);
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
